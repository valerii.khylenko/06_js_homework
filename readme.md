## Теоретичні питання

1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
2. Які засоби оголошення функцій ви знаєте?
3. Що таке hoisting, як він працює для змінних та функцій?

1. Екранування - це можливість використання спеціальних символів, як звичайних.
2. Функцію можливо оголосити де кільками засобами:
   Function declaration - function doSomething() { //to do something }
   Function expression - const doSomething = function() {//to do somethin}
   Arrow function - const doSomething = foo => {//to do something}
3. Hosting - це підйом локальних змінних функції на початок функції.