function createNewUser() {

    const newUser = {

        firstName: prompt('Enter your firstname?'),
        lastName: prompt('Enter your lastname?'),
        birthDayUser: prompt('Enter date of birth?'),

        getLogin: function () {
            let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
            return newLogin;
        },
        getAge: function () {
            let currentDate = new Date();
            let birthDay = this.birthDayUser.slice(0, 2);
            let birthMonth = this.birthDayUser.slice(3, 5);
            let birthYear = this.birthDayUser.slice(6, 10);

            let ageUser = currentDate.getFullYear() - birthYear;
            let m = currentDate.getMonth() - birthMonth;
            if (m < 0 || (m === 0 && currentDate.getDate() < birthDay)) {
                ageUser--;
            }
            return ageUser;
        },
        getPassword: function () {
            let userPassword = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthDayUser.slice(6, 10);
            return userPassword;
        },
    }
    return newUser;
}
newUser1 = createNewUser();
console.log(newUser1.getLogin());
console.log(newUser1.getAge());
console.log(newUser1.getPassword());
